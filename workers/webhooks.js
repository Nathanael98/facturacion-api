'use strict';

module.exports = function(){
	// Utils
	let _ = require('underscore');
	let Promise = require('promise');
	const request = require('request');
	
	function successRequest(url, json) {
		return new Promise(function(resolve, reject) {
			request.post(
				url, 
				{json: json},
				function(error, response, body) {
					if (error) {
						reject({
							error: error,
							response: response,
							body: body
						});
					} else {
						resolve();
					}
				}
			);
		});
	}
	
	return {
		success: successRequest
	};
}();





