module.exports = function () {
	let db = require("../config/admin").db
	let util = require('util')




	db.ref('cancelarRequests').orderByChild('status').equalTo(null).on('child_added', onCancelarRequestAdded)




	function onCancelarRequestAdded(cancelarRequestSnap) {
		let db = require("../config/admin").db;

		let cancelarRequest = cancelarRequestSnap.val(),
			facturaId = cancelarRequest.factura,
			accountId = cancelarRequest.account;

		if (!facturaId || !accountId || cancelarRequest.status == 2) {
			return
		};

		let accountRef = db.ref('accounts').child(accountId);
		let facturaRef = accountRef.child('facturas').child(facturaId);

		return facturaRef.once('value').then(onFactura).then(function () {
			accountRef = null;
			facturaRef = null;
			db = null;
			cancelarRequestSnap.ref.update({
				status: 2
			});
			cancelarRequestSnap = null;
		}).catch(function(e){
			cancelarRequestSnap.ref.update({
				status: 3,
				error: `${util.inspect(e, false, null)}`
			})
			cancelarRequestSnap = null
		});
	}





	function onFactura(facturaSnap) {
		debugger
		let Promise = require('promise');
		return new Promise(function (resolve, reject) {

			let pac = require('../services/pacs/edicom');
			let Factura = require('../models/factura');

			return Factura.load(facturaSnap).then(function (factura) {
					return pac.cancelar(factura).then(function (result) {
							debugger

							let exportable = require('../exportable')
							if (!result || !result.doc) {
								return reject({ phase: 'afterCancelar', error: 'No xml response' })
							}

							return exportable.buildXML(facturaSnap, `${result.doc}`).then(function (url) {
								return facturaSnap.ref.update({
									xmlCanceladoUrl: url,
									fechaCancelado: result.fechaCancelado
								}).catch(reject)
							}).then(function(){
								return facturaSnap.ref.once('value')
							}).then(function (facturaSnap) {
								return exportable.updatePDF(facturaSnap)
							}).catch(reject)
						})
						.then(function () { return facturaSnap.ref.once('value') })
						.then(function () { return sendWebhook(facturaSnap.ref) })
						.catch(reject)
				})
				.then(resolve)
				.catch(reject)
		});
	}






	function sendWebhook(facturaRef) {
		//console.log('cancelar webhook');
		return facturaRef.once('value', function (facturaSnap) {
			let factura = facturaSnap.val();
			let webhookUrl = factura.webhookUrl || factura.successWebhookUrl; // || 'http://localhost:4000/dummy/webhook';
			if (webhookUrl) {
				let webhookWorker = new require('./webhooks');
				let facturaJson = require('../serializers/factura-json');

				new facturaJson(facturaSnap).serialize().then(function (json) {
					webhookWorker.success(webhookUrl, json).then(function () {
						facturaSnap.ref.update({
							webhookSent: true
						});

						webhookWorker = null;
						facturaJson = null;
					});
				});
			}
		});
	}

}();
