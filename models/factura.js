module.exports = function () {
	let _ = require('underscore')
	let Promise = require('promise')
	let path = require('path')
	let rootPath = path.resolve('')
	var Decimal = require('decimal.js-light')

	return {
		load: loadData
	}


	function loadData(snap) {
		//console.log("cargando Data")
		return new Promise(function (resolve, reject) {
			let ref = snap.ref.parent.parent,
				factura = snap.val()

			factura.ref = snap.ref
			factura.id = snap.key

			function getRelated(collection, id) {
				if (!id) {
					return Promise.resolve()
				}

				if (typeof id === 'string') {
					return loadRecord(collection, id)
				} else {
					let ids = Array.isArray(id) ? id : Object.keys(id ||  {})
					return Promise.all(ids.map(function (i) {
						return loadRecord(collection, i)
					}))
				}
			}


			function loadRecord(collection, id) {
				return new Promise(function (resolve, reject) {
					return ref.child(collection).child(id).once('value').then(function (snap) {
						let obj = snap.val()
						if(obj){ obj['id'] = id}
						return resolve(obj)
						// return resolve(snap.val())
					}).catch(reject)
				})
			}


			try {
				// Fiscal Certificado data
				function getCertificado() {
					return getRelated('fiscalCertificados', factura.certificado)
				}



				// Fiscal Receptor data
				function getReceptor() {
					return getRelated('fiscalReceptores', factura.receptor).then(function (receptor) {
						return new Promise(function (resolve, reject) {

							// Relations of receptor
							return Promise.all([
								getDirecciones(receptor)
							]).then(function () {
								resolve(receptor)
							}).catch(reject)
						})
					})
				}



				// Fiscal Emisor data
				function getEmisor() {
					return getRelated('fiscalEmisores', factura.emisor).then(function (emisor) {
						return new Promise(function (resolve, reject) {

							// Relations of emisor
							return Promise.all([
								getDirecciones(emisor)
							]).then(function () {
								resolve(emisor)
							}).catch(reject)
						})
					})
				}


				// Fiscal Direcciones
				function getDirecciones(model) {
					return getRelated('fiscalDireccions', model.fiscalDirecciones).then(function (arr) {
						model.fiscalDirecciones = arr
					})
				}


				// Conceptos data
				function getConceptos() {
					//console.log("tomando los conceptos")
					return getRelated('conceptos', factura.conceptos).then(function (conceptos) {
						return new Promise(function (resolve, reject) {
							return Promise.all(
								conceptos.map(getConcepto)
							).then(function () {
								resolve(conceptos)
							}).catch(reject)
						})
					})
				}



				function processSingleImpuesto(impuesto) {
					impuesto.impuestoLabel = function () {
						switch (impuesto.impuesto) {
							case '001':
								return `ISR`
							case '002':
								return `IVA`
							case '003':
								return `IEPS`
							default:
								return `${impuesto.impuesto}`
						}
					}()
				}


				function getConcepto(concepto) {
					let promises = []
					promises.push(getRelated('impuestos', concepto.impuestos))

					if (concepto.iedu) {
						promises.push(getRelated('complementoConceptos/iedus', concepto.iedu))
					}

					return Promise.all(promises).then(function (arr) {
						concepto.impuestos = arr[0] || []
						concepto.impuestos.forEach(function (impuesto) {
							processSingleImpuesto(impuesto)


							if (impuesto.local) {
								factura.impuestosLocales = factura.impuestosLocales || []
								factura.impuestosLocales.push(impuesto)
							}
						})
						concepto.iedu = arr[1]
					})
				}


				// Impuestos Globales data
				function getImpuestosGlobales() {
					//console.log("obteniendo globales")
					return getRelated('impuestos', factura.impuestosGlobales)
				}



				function getXml() {
					let axios = require('axios')

					if (!factura.xmlUrl) { return Promise.resolve(null) }

					return axios.get(`${factura.xmlUrl}`).then(function (response) {
						let libxml = require('libxslt').libxmljs
						return libxml.parseXmlString(response.data, { noblanks: true })
					})
				}



				function setCadenaOriginalSat() {
					return new Promise(function (res, rej) {
						try {
							if (!factura.xmlDoc) { throw 'Origina XML, loaded as xmlDoc, file is required' }

							// Extractor de Cadena
							let xsltFile = path.resolve(rootPath, 'config/zfactu/sat/cadenaoriginal_TFD_1_1.xslt')

							// Timbre fiscal SAT
							let timbre = factura.xmlDoc.find('//tfd:TimbreFiscalDigital', {
								tfd: "http://www.sat.gob.mx/TimbreFiscalDigital"
							})[0]

							// debugger

							let libxslt = require('libxslt')
							libxslt.parseFile(xsltFile, function (err, stylesheet) {
								if (err) { rej(err) } else {
									stylesheet.apply(`${timbre}`, function (err, result) {
										if (err) { rej(err) } else {

											factura.cadenaOriginalSat = result
											res(result)
										}
									})
								}
							})

							if(timbre){
								factura.noCertificadoSAT = `${timbre.find('string(@NoCertificadoSAT)')}`
								factura.rfcProvCertif = `${timbre.find('string(@RfcProvCertif)')}`
								factura.selloSAT = `${timbre.find('string(@SelloSAT)')}`
								factura.selloCFD = `${timbre.find('string(@SelloCFD)')}`
							}
						} catch (err) { rej(err) }
					})
				}




				function processImpuestos() {
					debugger
					if (factura.impuestosGlobales.length > 0) {
						let globalesFederales = []

						factura.impuestosGlobales.forEach(function (impuesto) {
							////console.log(impuesto)
							if (impuesto.local) {
								factura.impuestosLocales = factura.impuestosLocales || []
								let imp = { ...impuesto }
								processSingleImpuesto(imp)
								factura.impuestosLocales.push(imp)
							} else {
								globalesFederales.push(impuesto)
							}
						})

						if (factura.conceptos.length > 0) {
							factura.conceptos.forEach(function (concepto) {
								concepto.impuestos = concepto.impuestos || []
								concepto.impuestos.push(...globalesFederales.map((impuesto) => {
									////console.log(impuesto)
									let imp = { ...impuesto }
									processSingleImpuesto(imp)
									imp.base = concepto.importe
									imp.importe = Decimal(imp.base).mul(impuesto.tasaCuota || 1).div(100).toNumber()
									return imp
								}))

								concepto.impuestos.forEach((impuesto) => {
									if (impuesto.local) {
										factura.impuestosLocales = factura.impuestosLocales || []
										let imp = { ...impuesto }
										processSingleImpuesto(imp)
										factura.impuestosLocales.push(imp)
									}
								});
							})
						}
					}

					factura.impTrasladadosGrouped = {}
					factura.impRetenidosGrouped = {}

					factura.conceptos.forEach(function (concepto) {
						concepto.impuestos.forEach(function (impuesto) {
							let key = [impuesto.impuesto, impuesto.tasaCuota, impuesto.tipoFactor].join('_')
							let obj

							switch (Number(impuesto.tipo)) {
								case 2:
									obj = factura.impTrasladadosGrouped
									break
								case 1:
									obj = factura.impRetenidosGrouped
									break
							}

							obj[key] = obj[key] || []
							obj[key].push(impuesto)
						})
					})
				}




				function getPdfTemplate() {
					return new Promise(function (resolve) {
						try {
							function onTemplate(snap) {
								if (!snap.val()) { return resolve(null) }
								let id = Object.keys(snap.val()).slice(-1)[0]
								return getRelated('facturaTemplates', id)
							}

							let templateRef, templateId = factura.templateId
							if (templateId) {
								return templateRef = ref.child('facturaTemplates')
									.child(templateId)
									.once('value')
									.then((snap)=> snap.val())
									.then(resolve)
									.catch(() => { debugger; resolve(null) })
							} else {
								let code = factura.templateCode || 'basic'
								return templateRef = ref.child('facturaTemplates')
									.orderByChild('code')
									.equalTo(code)
									.once('value')
									.then(onTemplate)
									.then(resolve)
									.catch(() => { debugger; resolve(null) })
							}
						} catch (e) { debugger; resolve(null) }
					})
				}


				function getComplementoIne(){
					debugger

					function replaceTipoProceso(clave){
						switch(clave){
							case '1': 
								return 'Precampaña'
							case '2': 
								return 'Ordinario'
							case '3': 
								return 'Campaña'
						}
					}

					return new Promise(function(resolve){
						try{
							debugger
							if(!factura.ine){ 
								resolve(null) 
							}

							return getRelated('complemento/ines', factura.ine).then(function(ine){

								ine.tipoProceso = replaceTipoProceso(ine.tipoProceso)
								
								if(ine.entidades){
									return getRelated('complemento/ine/entidads', ine.entidades).then(function(entidades){
										ine.entidades = entidades || []

										return Promise.all(ine.entidades.map(function(entidad){
											return getRelated('complemento/ine/contabilidads', entidad.contabilidades).then(function(contabilidades){
												entidad.contabilidades = contabilidades || []
											})
										}))
									}).done(function(){
										return resolve(ine)
									})
								}else{
									return resolve(ine)
								}
							})
						}catch(e){ 
							debugger
							resolve(null) 
						}
					})
				}



				return Promise.all([
						getCertificado(), // 0
						getReceptor(), // 1
						getEmisor(), // 2
						getConceptos(), // 3
						getImpuestosGlobales(), // 4
						getXml(), // 5
						getPdfTemplate(), //6
						getComplementoIne() // 7
					]).then(function (arr) {

						factura.certificado = arr[0]
						factura.receptor = arr[1]
						factura.emisor = arr[2]
						factura.conceptos = _.sortBy(arr[3] || [], 'id') //factura.conceptos = arr[3] || []

						factura.impuestosGlobales = arr[4] || []
						processImpuestos()


						if (arr[6]) { factura.template = arr[6] }
						if(arr[7]){  factura.ine = arr[7] }

						// estas deben ser las últimas lineas del método
						if (arr[5]) { factura.xmlDoc = arr[5] }
						if (factura.xmlDoc) {
							return setCadenaOriginalSat()
						}
					})
					.then(() => resolve(factura))
					.catch(reject)

			} catch (e) {
				reject(e)
			}
		})
	}
}()
