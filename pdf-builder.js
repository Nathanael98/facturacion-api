module.exports = function () {
  let path = require('path')
  let rootPath = path.resolve('')



  function getTemplate(name) {
    return require('./views/pdf/templates/' + name)
  }



  async function getLogoAsUri(factura) {
    return new Promise(function (resolve, reject) {
      if(!factura.template || !factura.template.logo){
        return resolve(null)
      }

      let tmp = require('tmp')
      tmp.file(function _tempFileCreated(error, path) {
        if (error) { console.log(error); return resolve(null) }

        let fs = require('fs')
        let request = require('request')
        request(factura.template.logo, function (error, response, body) {
          if (error) { console.log(error); return resolve(null) }

          // Returns logo as dataUri
          let DataURI = require('datauri').promise
          return DataURI(path).then(resolve);
        }).pipe(fs.createWriteStream(path))
      })
    })
  }
  async function getQRAsURI(factura){
    return new Promise(function (resolve, reject) {
    let number = factura.selloCFD
    let url ="https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx?" + "&id=" + factura.uuid +"&re="+factura.emisor.rfc +"&rr=" + factura.receptor.rfc + "&tt=" + factura.total + "&fe=" + number.substring(number.length-8, number.length)
    let request = require('request')

    let rootUrl = 'https://chart.googleapis.com/chart'

    let jsonDataObj = {
      cht: 'qr',
      chs: '545x545',
      chl: url
    }

    let tmp = require('tmp')
      tmp.file(function _tempFileCreated(error, path) {
        if (error) { console.log(error); return resolve(null) }

        let fs = require('fs')
        let request = require('request')
        request.post({
          headers: {'content-type':'application/json'},
          url: rootUrl, 
          form: jsonDataObj,

        }, function (error, response, body) {

          if (error) { console.log(error); return resolve(null) }

          // Returns qr as dataUri
          let DataURI = require('datauri').promise
          return DataURI(path).then(resolve);
        }).pipe(fs.createWriteStream(path))
      })
    });
  }



  return async function (factura, templateName) {
    // process.chdir(path.resolve(rootPath, './'));
    let fonts = {
      HelveticaNeue: {
        normal: global.rootPath('./views/pdf/fonts/HelveticaNeue-01.ttf'),
        bold: global.rootPath('./views/pdf/fonts/HelveticaNeue-Bold-02.ttf'),
        italics: global.rootPath('./views/pdf/fonts/HelveticaNeue-Italic-03.ttf'),
        bolditalics: global.rootPath('./views/pdf/fonts/HelveticaNeue-BoldItalic-04.ttf')
      }
    }

    // Utils
    let _ = require('underscore')
    let PdfPrinter = require('pdfmake')
    let printer = new PdfPrinter(fonts)

    // Firebase Admin
    let admin = require("./config/admin").app
    let bucket = admin.storage().bucket()
    let db = require("./config/admin").db

    let template = getTemplate(templateName)
    let defaultDd = {images:{}}

    defaultDd.images.logo = await getLogoAsUri(factura)
    defaultDd.images.qr = await getQRAsURI(factura)

    return printer.createPdfKitDocument(await template.render(factura, defaultDd))
  };
}();


//module.exports();
