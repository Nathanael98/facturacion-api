var admin = require("firebase-admin");


// Datos Estaticos Para generar el pdf y poder obetener los datos correspondientes
//let id_facturas = ["-L7RgmzwguTOJbf4FS49", "-L76eZ6RoZmI4XIwGrFS"]
let id_facturas = ["-L7TSJiI6Hpmmv2IvI6W"]
var serviceAccount = require('./config/service_accounts/admin.json');

let path = require('path')
global._rootPath = path.resolve('')
global.rootPath = function(){ return require('path').resolve(global._rootPath, ...arguments) }

// As an admin, the app has access to read and write all data, regardless of Security Rules
let db = require(global.rootPath("config/admin")).db;

//Es un librearia la cual sirve para igualar una cadena en este caso "string" a un arreglo
let _ = require('underscore')
var msgPdfUrl = []
var ref1;
var ref2;

require('./config/initializers/string')
require('./config/initializers/number-to-words')

/*
return function (id_facturas) {
 if (!_.isArray(id_facturas)) {
   id_facturas = [id_facturas]
 }
 */

let q = id_facturas.map(function (factura) {
  return arrayFacturas(factura)  
})

return Promise.all(q).then(() => {  
  console.log(msgPdfUrl)  
}).catch((e) => {
  console.log(e)
})

//Una apuesta a q no le marcas


function arrayFacturas(id_factura) {

  ref = db.ref(`facturas/${id_factura}`);  

  return ref.once("value").then(function (snapshot) {
    
    ref = db.ref(snapshot.val().path);
    return ref.once("value", function (snapshot) {

      //Generar el .once despues de la funcion generar pdf      
      return generarPDF(snapshot).then(msgPdfUrl.push(urlPdfValidar(snapshot)))
      
    });
  });
}

function urlPdfValidar(factura) {
  let id, status, msg, error, objeto
  let id_factura = factura.key, urlPdf = factura.val().pdfUrl

  if (urlPdf === undefined) {
    objeto = {
      id: id_factura,
      status: 3,
      msg: 'El campo urlPdf no existe'
    }
  } else if (urlPdf === "") {
    objeto = {
      id: id_factura,
      status: 3,
      msg: 'El PDF no pudo ser generado',
      error: 'Hubo un problema al generar la factura'
    }
  } else {
    objeto = {
      id: id_factura,
      status: 2,
      msg: 'El PDF fue generado correctamente'
    }
  }
  return objeto
}

function generarPDF(factu) {
  
  
  console.log(" generando PDF" + "\n")

  //console.log(factu);
  let exportable = require('./exportable');  
  return exportable.buildPDF(factu).then(function (url) {    
    console.log(url)    
  
    return factu.ref.update({ pdfUrl: url }).then(function () {      
      return factu.ref.once('value');
            

    }).catch((e)=>{
      console.log(e)
    });
  }).catch((e)=>{        
    console.log(e)       
   });

}