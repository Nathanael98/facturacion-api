// Firebase Admin
const admin = require("firebase-admin");

let credentialsObj;

if (process.env.env == 'production'){
  credentialsObj = {
    credential: admin.credential.cert(require("./service_accounts/admin.json")),
    databaseURL: "https://contamc-facturacion.firebaseio.com",
    storageBucket: "contamc-facturacion.appspot.com"
  }
} else {
  credentialsObj = {
    credential: admin.credential.cert(require("./service_accounts/admin-dev.json")),
    databaseURL: "https://contamc-facturacion-dev.firebaseio.com",
    storageBucket: "contamc-facturacion-dev.appspot.com"
  }
}

admin.initializeApp(credentialsObj);

exports.db = admin.database();
exports.app = admin;