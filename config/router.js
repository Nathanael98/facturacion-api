// Express
const express = require('express');
const bodyParser = require('body-parser');


module.exports = function() {
	let app = express();

    app.use(bodyParser.json()); // support json encoded bodies
    app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies



    // POST /auth
    let loginController = require('../controllers/login');
    app.post('/auth', loginController.auth);



    // POST /dummy/webhook
    app.post('/dummy/webhook', function(req, res) {
        return res.send({
            sentData: req.body
        });
    });



    // GET /dummy
    app.get('/dummy', function(req, res) {
        return res.send('Hello!');
    });



    const PORT = process.env.env == 'production' ? 4000 : 4001;

    // Server start
    app.listen(PORT, function (){
        //console.log('ContaMC server started ' + PORT);
    });
}
