module.exports = function() {
	return {
		getName: getName
	}

	async function getName(catalogue, code) {
		let axios = require('axios')
		let Promise = require('promise')


		let host = 'https://elastic.facturacion.apprendia.tech/sat3_3'
		let url = `${host}/${catalogue}/${code}/_source`

		return await axios.get(url).then(function(response) {
			return response.data.name
		}).catch(function(e) {
			//console.log(e)
			return ''
		})
	}
}()

// https://elastic.contamc.com/sat3_3/c_ClaveUnidad/E48/_source
