'use strict';

module.exports = function() {
  return {
    timbrar: require('./edicom/timbrar'),
    cancelar: require('./edicom/cancelar'),
    retrieve: require('./edicom/retrieve')
  }
}()
