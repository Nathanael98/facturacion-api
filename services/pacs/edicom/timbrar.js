'use strict';

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

module.exports = function () {
	// Utils
	// const _ = require('underscore')

	// Constants
	let { usr, pwd } = require('./constants')
	let Promise = require('promise')



	// Timbrar entry point
	return function (xml, opts) {
		opts = opts || {}
		let testOnly = (process.env.env != 'production') || opts.prueba || opts.testOnly

		return sendRequest(xml, testOnly, opts.id).then(onTimbrado)
	}




	// Actually send the request to SOAP
	function sendRequest(xml, testOnly, id) {
		try {
			let libxml = require('libxslt').libxmljs
			let fs = require('fs');
			// let soap = require('soap');

			let args = {
				user: usr,
				password: pwd,
				file: Buffer.from(xml).toString('base64')
			}


			let method = testOnly ? 'getCfdiTest' : 'getCfdi'
			let connection = require('./connection')

			return connection.connect().then(function (client) {
				// Call the method, returns a Promise

				try {
					return client[`${method}Async`](args).catch(function (e) {
						return Promise.reject(e)
					})
				} catch (e) {
					return Promise.reject(e)
				}

			}).then(function (result) {
				let response = result[`${method}Return`]
				let zip = new require('node-zip')(response, {
					base64: true,
					checkCRC32: true
				})


				let comprobante = zip.files['SIGN_XML_COMPROBANTE_3_0.xml']
				let resultDoc = libxml.parseXmlString(comprobante.asText(), {
					noblanks: true
				})


				let timbre = resultDoc.get('//tfd:TimbreFiscalDigital', {
					tfd: 'http://www.sat.gob.mx/TimbreFiscalDigital'
				})

				//console.log('sendRequest factura ', id)


				return {
					doc: resultDoc,
					timbre: timbre
				}
			}).catch(function (e) {
				return Promise.reject(e)
			})
		} catch (e) {
			return Promise.reject(e)
		}

		// let xmlEnvelope = new libxml.Document();
		// let envelope = xmlEnvelope.node('Envelope');

		// envelope.namespace(envelope.defineNamespace('soapenv', 'http://schemas.xmlsoap.org/soap/envelope/'));
		// let cfdiNS = envelope.defineNamespace('cfdi', 'http://www.sat.gob.mx/cfd/3');
	}




	// Timbrado successful!
	function onTimbrado(result) {
		let moment = require('moment-timezone')

		return {
			uuid: result.timbre.attr('UUID').value(),
			fechaTimbrado: moment.tz(result.timbre.attr('FechaTimbrado').value(), 'America/Mexico_City').utc().format(),
			xml: result.doc
		}
	}
}()
